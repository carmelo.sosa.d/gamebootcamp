package com.bootcamp.game.app.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.bootcamp.game.app.dtos.GenreEnum;
import com.bootcamp.game.app.model.entity.Genre;

@Component
public class GenreToGenreEnum implements Converter<Genre, GenreEnum> {

	@Override
	public GenreEnum convert(Genre source) {
		GenreEnum destino = source.getNombre();				
		return destino;
	}
	
	

}
