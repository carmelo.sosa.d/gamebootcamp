package com.bootcamp.game.app.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.bootcamp.game.app.dtos.GameRequest;
import com.bootcamp.game.app.model.entity.Game;

@Component
public class GameRequestToGame implements Converter<GameRequest, Game> {

	@Override
	public Game convert(GameRequest source) {
		Game destino = new Game();
		destino.setTitle(source.getTitle());
		destino.setDescription(source.getDescription());
		destino.setRelease(source.getRelease());
				
		return destino;
	}

}
