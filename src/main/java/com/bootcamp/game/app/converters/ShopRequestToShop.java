package com.bootcamp.game.app.converters;

import org.springframework.core.convert.converter.Converter;
import com.bootcamp.game.app.dtos.ShopRequest;
import com.bootcamp.game.app.model.entity.Shop;


public class ShopRequestToShop implements Converter<ShopRequest, Shop> {

	@Override
	public Shop convert(ShopRequest source) {
		Shop destino = new Shop();
		destino.setNombre(source.getNombre());
		destino.setDomicilio(source.getDomicilio());
				
		return destino;
	}

}
