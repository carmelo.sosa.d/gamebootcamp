package com.bootcamp.game.app.converters;

import java.util.Optional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.bootcamp.game.app.dtos.GenreEnum;
import com.bootcamp.game.app.exceptions.GenreKONotExistException;
import com.bootcamp.game.app.model.entity.Genre;
import com.bootcamp.game.app.services.GenreService;


@Component
public class GenreEnumToGenre implements Converter<GenreEnum, Genre> {

	private GenreService genreService;
	
	public GenreEnumToGenre(GenreService genreService) {
		this.genreService = genreService;
	}
	
	@Override
	public Genre convert(GenreEnum source) {
		Optional<Genre> generoBd = genreService.checkGenreIfExist(source);
		if (generoBd.isPresent())
			return generoBd.get();
		else {
			throw new GenreKONotExistException();
		}
	}
	
	

}
