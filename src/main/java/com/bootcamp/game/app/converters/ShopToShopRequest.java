package com.bootcamp.game.app.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.bootcamp.game.app.dtos.ShopRequest;
import com.bootcamp.game.app.model.entity.Shop;

@Component
public class ShopToShopRequest implements Converter<Shop, ShopRequest> {

	@Override
	public ShopRequest convert(Shop source) {
		ShopRequest destino = new ShopRequest();
		destino.setNombre(source.getNombre());
		destino.setDomicilio(source.getDomicilio());
				
		return destino;
	}

}
