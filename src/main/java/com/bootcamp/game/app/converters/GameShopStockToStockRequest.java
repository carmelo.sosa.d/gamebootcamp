package com.bootcamp.game.app.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.bootcamp.game.app.dtos.StockRequest;
import com.bootcamp.game.app.model.entity.GameShopStock;

@Component
public class GameShopStockToStockRequest implements Converter<GameShopStock, StockRequest> {

	@Override
	public StockRequest convert(GameShopStock source) {
		StockRequest destino = new StockRequest();
		destino.setCantidad(source.getCantidad());
		destino.setTitleGame(source.getGame().getTitle());
			
		return destino;
	}

}
