package com.bootcamp.game.app.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.bootcamp.game.app.dtos.GameRequest;
import com.bootcamp.game.app.model.entity.Game;

@Component
public class GameToGameRequest implements Converter<Game, GameRequest> {

	@Override
	public GameRequest convert(Game source) {
		GameRequest destino = new GameRequest();
		destino.setTitle(source.getTitle());
		destino.setDescription(source.getDescription());
		destino.setRelease(source.getRelease());
				
		return destino;
	}

}
