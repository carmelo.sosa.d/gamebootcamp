package com.bootcamp.game.app.exceptions;

import com.bootcamp.game.app.exceptions.generic.ShopKOException;

public class ShopKONotExistException extends ShopKOException {


	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "Tienda no existe";

	public ShopKONotExistException(String detalle) {
		super(detalle);		
	}
	
	public ShopKONotExistException() {
		super(DETAIL);
	}

}
