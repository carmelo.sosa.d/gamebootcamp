package com.bootcamp.game.app.exceptions;

import com.bootcamp.game.app.exceptions.generic.ShopKOException;

public class ShopKODuplicadoException extends ShopKOException {


	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "Ya existe una tienda con el mismo nombre";

	public ShopKODuplicadoException(String detalle) {
		super(detalle);		
	}
	
	public ShopKODuplicadoException() {
		super(DETAIL);
	}

}
