package com.bootcamp.game.app.exceptions;

import com.bootcamp.game.app.exceptions.generic.GameKOException;

public class GameKONotExistException extends GameKOException {


	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "Juego no existe";

	public GameKONotExistException(String detalle) {
		super(detalle);		
	}
	
	public GameKONotExistException() {
		super(DETAIL);
	}

}
