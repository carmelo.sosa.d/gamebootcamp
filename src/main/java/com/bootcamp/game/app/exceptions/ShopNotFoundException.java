package com.bootcamp.game.app.exceptions;

import com.bootcamp.game.app.exceptions.generic.NotFoundExecption;

public class ShopNotFoundException extends NotFoundExecption{

	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "Tienda no encontrada";
	
	
	public ShopNotFoundException(String detalle) {
		super(detalle);
	}

	public ShopNotFoundException() {
		super(DETAIL);
	}
}
