package com.bootcamp.game.app.exceptions;

import com.bootcamp.game.app.exceptions.generic.GenreKOException;

public class GenreKONotExistException extends GenreKOException {


	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "Género no existe";

	public GenreKONotExistException(String detalle) {
		super(detalle);		
	}
	
	public GenreKONotExistException() {
		super(DETAIL);
	}

}
