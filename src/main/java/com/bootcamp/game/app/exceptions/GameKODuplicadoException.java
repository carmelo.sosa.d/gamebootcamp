package com.bootcamp.game.app.exceptions;

import com.bootcamp.game.app.exceptions.generic.GameKOException;

public class GameKODuplicadoException extends GameKOException {


	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "Ya existe un juego con el mismo nombre";

	public GameKODuplicadoException(String detalle) {
		super(detalle);		
	}
	
	public GameKODuplicadoException() {
		super(DETAIL);
	}

}
