package com.bootcamp.game.app.exceptions;

import com.bootcamp.game.app.exceptions.generic.ShopKOException;

public class ShopKOStockException extends ShopKOException {


	private static final long serialVersionUID = 1L;
	
	private static final String DETAIL = "No existe suficiente stock";

	public ShopKOStockException(String detalle) {
		super(detalle);		
	}
	
	public ShopKOStockException() {
		super(DETAIL);
	}

}
