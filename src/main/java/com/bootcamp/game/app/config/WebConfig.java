package com.bootcamp.game.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.bootcamp.game.app.converters.GameRequestToGame;
import com.bootcamp.game.app.converters.GameShopStockToStockRequest;
import com.bootcamp.game.app.converters.GameToGameRequest;
import com.bootcamp.game.app.converters.GenreEnumToGenre;
import com.bootcamp.game.app.converters.GenreToGenreEnum;
import com.bootcamp.game.app.converters.ShopRequestToShop;
import com.bootcamp.game.app.converters.ShopToShopRequest;
import com.bootcamp.game.app.services.GenreService;

@Configuration
public class WebConfig implements WebMvcConfigurer {
	
	@Autowired
	private GenreService genreService;

	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new GameRequestToGame());
		registry.addConverter(new GameToGameRequest());
		registry.addConverter(new GenreToGenreEnum());
		registry.addConverter(new GenreEnumToGenre(genreService));
		registry.addConverter(new ShopRequestToShop());
		registry.addConverter(new ShopToShopRequest());
		registry.addConverter(new GameShopStockToStockRequest());
	}
}
