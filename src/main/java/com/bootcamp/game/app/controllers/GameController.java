package com.bootcamp.game.app.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import com.bootcamp.game.app.dtos.GameRequest;
import com.bootcamp.game.app.exceptions.GameKONotExistException;
import com.bootcamp.game.app.services.GameService;

@RestController
public class GameController {
	
	@Autowired
	private GameService gameService;
	
	@GetMapping("/game")
	public ResponseEntity<Object> getGame(@RequestParam String title, HttpRequestHandlerServlet request) throws GameKONotExistException{
		
		return ResponseEntity.ok().body(gameService.getGame(title));
	}
	
	
	@PostMapping("/game")
	public ResponseEntity<Object> addGame(@RequestBody @Valid GameRequest game, HttpRequestHandlerServlet request) {

		return ResponseEntity.ok().body(gameService.addGame(game));
	}
	
	@PostMapping("/game/update")
	public ResponseEntity<Object> updateGame(@RequestBody @Valid GameRequest game, HttpRequestHandlerServlet request) {

		return ResponseEntity.ok().body(gameService.updateGame(game));
	}
	
	@GetMapping("/game/delete")
	public ResponseEntity<Object> deleteGame(@RequestParam String title, HttpRequestHandlerServlet request) throws GameKONotExistException{
		
		return ResponseEntity.ok().body(gameService.deleteGame(title));
	}
	
	
}
