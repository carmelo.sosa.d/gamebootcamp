package com.bootcamp.game.app.controllers;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import com.bootcamp.game.app.dtos.ShopRequest;
import com.bootcamp.game.app.exceptions.ShopKONotExistException;
import com.bootcamp.game.app.services.ShopService;

@RestController
public class ShopController {
	
	@Autowired
	private ShopService shopService;
	
	@GetMapping("/shop")
	public ResponseEntity<Object> getShop(@RequestParam String nombre, HttpRequestHandlerServlet request) throws ShopKONotExistException{
		
		return ResponseEntity.ok().body(shopService.getShop(nombre));
	}
	
	
	@PostMapping("/shop")
	public ResponseEntity<Object> addShop(@RequestBody @Valid ShopRequest shop, HttpRequestHandlerServlet request) {

		return ResponseEntity.ok().body(shopService.addShop(shop));
	}
	
	@PostMapping("/shop/update")
	public ResponseEntity<Object> updateShop(@RequestBody @Valid ShopRequest shop, HttpRequestHandlerServlet request) {

		return ResponseEntity.ok().body(shopService.updateShop(shop));
	}
	
	@GetMapping("/shop/delete")
	public ResponseEntity<Object> deleteShop(@RequestParam String nombre, HttpRequestHandlerServlet request) throws ShopKONotExistException{
		
		return ResponseEntity.ok().body(shopService.deleteShop(nombre));
	}
	
	@GetMapping("/shop/addstock")
	public ResponseEntity<Object> addStockShop(@RequestParam @NotNull String nameShop, @RequestParam @NotNull String titleGame, @RequestParam @Min(value = 1, message = "El stock debe ser un valor mayor o igual a 1") int cantidad, HttpRequestHandlerServlet request) throws ShopKONotExistException{
		
		return ResponseEntity.ok().body(shopService.addStock(nameShop, titleGame, cantidad));
	}
	
	@GetMapping("/shop/removestock")
	public ResponseEntity<Object> removeStockShop(@RequestParam @NotNull String nameShop, @RequestParam @NotNull String titleGame, @RequestParam @Min(value = 1, message = "El stock debe ser un valor mayor o igual a 1") int cantidad, HttpRequestHandlerServlet request) throws ShopKONotExistException{
		
		return ResponseEntity.ok().body(shopService.removeStock(nameShop, titleGame, cantidad));
	}
}
