package com.bootcamp.game.app.model.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.game.app.model.entity.Game;

public interface GameRepository extends JpaRepository<Game, Long> {

	public Optional<Game> findByTitle(String title);
}
