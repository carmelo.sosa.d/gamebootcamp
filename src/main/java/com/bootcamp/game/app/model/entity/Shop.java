package com.bootcamp.game.app.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity(name = "SHOPS")
@Transactional
@Data
public class Shop implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idshop;
	
	@Column(name = "nombre", unique = true)
	@NotNull
	private String nombre;
	
	@Column(name = "domicilio")
	@NotNull
	private String domicilio;
	
	
	@OneToMany(mappedBy="shop", cascade = CascadeType.ALL)
	private List<GameShopStock> gameShopStock;
}
