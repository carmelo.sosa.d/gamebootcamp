package com.bootcamp.game.app.model.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.game.app.model.entity.Game;
import com.bootcamp.game.app.model.entity.GameShopStock;
import com.bootcamp.game.app.model.entity.Shop;

public interface GameShopStockRepository extends JpaRepository<GameShopStock, Long> {

	public Optional<GameShopStock> findByGameAndShop(Game game, Shop shop);
	
}
