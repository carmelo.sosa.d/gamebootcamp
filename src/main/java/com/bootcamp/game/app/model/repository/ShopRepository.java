package com.bootcamp.game.app.model.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.game.app.model.entity.Shop;

public interface ShopRepository extends JpaRepository<Shop, Long> {

	public Optional<Shop> findByNombre(String nombre);
	
}
