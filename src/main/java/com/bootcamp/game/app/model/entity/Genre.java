package com.bootcamp.game.app.model.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import com.bootcamp.game.app.dtos.GenreEnum;

import lombok.Data;




@Entity(name="GENRES")
@Transactional
@Data
public class Genre implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idgenre;
	
	@Column(name="NOMBRE", unique = true)
	@Enumerated(EnumType.STRING)
	@NotNull	
	private GenreEnum nombre;
	
	@ManyToMany(mappedBy = "genres",fetch = FetchType.LAZY)
	private Set<Game> games;
	
	

}
