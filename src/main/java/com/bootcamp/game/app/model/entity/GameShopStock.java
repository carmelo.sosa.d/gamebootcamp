package com.bootcamp.game.app.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.transaction.Transactional;

import lombok.Data;

@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"idshop", "idgame"}))
@Entity(name = "GAMESHOPSTOCKS")
@Transactional
@Data
public class GameShopStock implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long idgameshopstock;
	
	@Column(name = "CANTIDAD")
	public int cantidad;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idshop")
    private Shop shop;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idgame")	
    private Game game;
	
}
