package com.bootcamp.game.app.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import lombok.Data;



@Entity(name="GAMES")
@Transactional
@Data
public class Game implements Serializable{


	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idgame;
	
	@Column(name="TITLE", unique = true)	
	@NotNull
	private String title;
	
	@Column(name="DESCRIPTION")
	@NotNull	
	private String description;
	
	@ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE}, fetch = FetchType.LAZY)
	@JoinTable(
            name = "game_genres",
            joinColumns = {@JoinColumn(name = "game_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")}
    )
	private Set<Genre> genres;
	
	@Column(name="RELEASE")
	@NotNull	
	private LocalDate release;
	
	@OneToMany(mappedBy="game", cascade = CascadeType.ALL)
	private List<GameShopStock> gameShopStock;
	
	
	public void addGenres(Genre genero) {
		genres.add(genero);
        genero.getGames().add(this);
    }
	
}
