package com.bootcamp.game.app.model.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.game.app.dtos.GenreEnum;
import com.bootcamp.game.app.model.entity.Genre;

public interface GenreRepository extends JpaRepository<Genre, Long> {

	public Optional<Genre> findByNombre(GenreEnum nombre);
	
}
