package com.bootcamp.game.app.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class StockRequest {
		
	@NotBlank(message="No acepta valores nulos o vacíos")
	private String titleGame;
	
	@NotNull(message="No acepta valores nulos")
	private int cantidad;	
	
	
	
}
