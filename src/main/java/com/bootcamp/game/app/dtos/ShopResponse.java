package com.bootcamp.game.app.dtos;


import java.time.LocalDate;

import lombok.Data;

@Data
public class ShopResponse {

	private String tienda;
	private LocalDate date;
	
	public ShopResponse(){
		
	}

	public ShopResponse(String tienda) {
		this.tienda = tienda;
		this.date = LocalDate.now();
	}
	
	
}
