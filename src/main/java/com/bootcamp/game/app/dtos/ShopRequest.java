package com.bootcamp.game.app.dtos;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class ShopRequest {
	
	@NotBlank(message="No acepta valores nulos o vacíos")
	private String nombre;
	
	@NotNull(message="No acepta valores nulos")
	@Size(min=2, max=100)
	private String domicilio;	
	
	private List<StockRequest> stocks;

	
}
