package com.bootcamp.game.app.dtos;

public enum GenreEnum {
	LUCHA,
	ARCADE,
	CARRERAS,
	SIMULADOR,
	DEPORTE
}
