package com.bootcamp.game.app.dtos;


import java.time.LocalDate;

import lombok.Data;

@Data
public class GameResponse {

	private String title;
	private LocalDate date;
	
	public GameResponse(){
		
	}

	public GameResponse(String title) {
		this.title = title;
		this.date = LocalDate.now();
	}
	
	
}
