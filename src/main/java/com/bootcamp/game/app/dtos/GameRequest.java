package com.bootcamp.game.app.dtos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bootcamp.game.app.model.entity.Genre;

import lombok.Data;

@Data
public class GameRequest {
	
	@NotBlank(message="No acepta valores nulos o vacíos")
	private String title;
	
	@NotNull(message="No acepta valores nulos")
	@Size(min=2, max=50)
	private String description;	
	private List<GenreEnum> genresreq = new ArrayList<>();
	private LocalDate release;
	
	public void setGenresGame(Set<Genre> generosBd) {
		for (Genre obj : generosBd) {
			this.genresreq.add(obj.getNombre());
		}
	}

	
}
