package com.bootcamp.game.app.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bootcamp.game.app.dtos.ShopRequest;
import com.bootcamp.game.app.dtos.ShopResponse;
import com.bootcamp.game.app.exceptions.ShopKONotExistException;
import com.bootcamp.game.app.model.entity.Game;
import com.bootcamp.game.app.model.entity.GameShopStock;
import com.bootcamp.game.app.model.entity.Shop;
import com.bootcamp.game.app.model.repository.GameShopStockRepository;
import com.bootcamp.game.app.model.repository.ShopRepository;
import com.bootcamp.game.app.services.GameService;
import com.bootcamp.game.app.services.ShopService;
import com.bootcamp.game.app.services.helper.GameHelper;
import com.bootcamp.game.app.services.helper.ShopHelper;

@Service
public class ShopServiceImpl implements ShopService {


	@Autowired
	private ShopRepository shopManager;
	
	@Autowired
	private GameShopStockRepository gameShopStockManager;
	
	@Autowired
	private GameService gameService;
	
	@Autowired
	private GameHelper gameHelper;
	@Autowired
	private ShopHelper shopHelper;	

	@Override
	public ShopResponse addShop(ShopRequest shoprequest) {
		shopHelper.checkearDuplicadoShop(getShopBd(shoprequest.getNombre()));
		
		Shop shop = shopHelper.convertShopRequestToShop(shoprequest,gameService);
		shopManager.save(shop);
		return new ShopResponse(shop.getNombre());
	}

	@Override
	public ShopRequest getShop(String nombre) {

		return shopHelper.convertIfExistShop(getShopBd(nombre));
	}

	@Override
	public ShopResponse updateShop(ShopRequest shoprequest) {
		Optional<Shop> shopBd = getShopBd(shoprequest.getNombre());
		if (shopBd.isPresent()) {
			Shop shop = shopHelper.convertShopRequestToShop(shoprequest,gameService);
			shop.setIdshop(shopBd.get().getIdshop());
			shop.getGameShopStock().stream().forEach(g -> g.setIdgameshopstock(obtenerIdStock(g.getGame(),g.getShop())));			
			shopManager.save(shop);
		}else {
			throw new ShopKONotExistException();
		}
		return new ShopResponse(shoprequest.getNombre());
	}

	@Override
	public ShopResponse deleteShop(String nombre) {
		Optional<Shop> shopBd = getShopBd(nombre);
		if (shopBd.isPresent()) {			
			shopManager.delete(shopBd.get());
		}else {
			throw new ShopKONotExistException();
		}		
		return new ShopResponse(nombre);
	}

	@Override
	public ShopResponse addStock(String nombre, String title, int cantidad) {

		return RealizarStock(nombre, title, cantidad, false);
	}

	@Override
	public ShopResponse removeStock(String nombre, String title, int cantidad) {

		return RealizarStock(nombre, title, cantidad, true);
	}

	@Override
	public Optional<Shop> getShopBd(String nombre) {

		return shopManager.findByNombre(nombre);
	}


	
	private ShopResponse RealizarStock(String nombre, String title, int cantidad, boolean esRemove) {
		Optional<Shop> shop = getShopBd(nombre);
		shopHelper.checkearExisteShop(shop);
		Optional<Game> game = gameService.getGameBd(title);
		gameHelper.checkearExisteGame(game);
		
		//Si stock existe actualiza cantidad sino crea un stock nuevo con la nueva cantidad
		Optional<GameShopStock> gameShopStock = gameShopStockManager.findByGameAndShop(game.get(), shop.get());
		gameShopStockManager.save(shopHelper.updateCantidadStock(gameShopStock, game, shop, cantidad, esRemove));
		return new ShopResponse(shop.get().getNombre());
	}
	 

	private Long obtenerIdStock(Game game, Shop shop) {
		return (gameShopStockManager.findByGameAndShop(game, shop)).get().getIdgameshopstock();
	}
	


}
