package com.bootcamp.game.app.services;

import java.util.Optional;

import com.bootcamp.game.app.dtos.ShopRequest;
import com.bootcamp.game.app.dtos.ShopResponse;
import com.bootcamp.game.app.model.entity.Shop;

public interface ShopService {

	public ShopResponse addShop(ShopRequest shop);
	public ShopRequest getShop(String nombre);
	public ShopResponse updateShop(ShopRequest shop);
	public ShopResponse deleteShop(String nombre);
	public ShopResponse addStock(String nombre, String title, int cantidad);
	public ShopResponse removeStock(String nombre, String title, int cantidad);
	public Optional<Shop> getShopBd(String nombre);
}
