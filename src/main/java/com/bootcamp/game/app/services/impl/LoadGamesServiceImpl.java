package com.bootcamp.game.app.services.impl;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bootcamp.game.app.dtos.GenreEnum;
import com.bootcamp.game.app.model.entity.Genre;
import com.bootcamp.game.app.services.GenreService;
import com.bootcamp.game.app.services.LoadGamesService;

@Service
public class LoadGamesServiceImpl implements LoadGamesService {

	@Autowired
	private GenreService genreService;
	
	@PostConstruct
	@Override
	public void LoadGenreEnumBd() {
		for (GenreEnum elemento : GenreEnum.values()) {
			Optional<Genre> generoBd = genreService.checkGenreIfExist(elemento);
			if (!generoBd.isPresent()) {
				genreService.addGenre(elemento);
			}
		}
	}

}
