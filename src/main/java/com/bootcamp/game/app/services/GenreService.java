package com.bootcamp.game.app.services;

import java.util.Optional;

import com.bootcamp.game.app.dtos.GenreEnum;
import com.bootcamp.game.app.model.entity.Genre;

public interface GenreService {

	public Optional<Genre> checkGenreIfExist(GenreEnum genre);
	public Genre addGenre(GenreEnum genre);
}
