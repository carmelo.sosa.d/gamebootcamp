package com.bootcamp.game.app.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bootcamp.game.app.dtos.GameRequest;
import com.bootcamp.game.app.dtos.GameResponse;
import com.bootcamp.game.app.exceptions.GameKONotExistException;
import com.bootcamp.game.app.model.entity.Game;
import com.bootcamp.game.app.model.repository.GameRepository;
import com.bootcamp.game.app.services.GameService;
import com.bootcamp.game.app.services.helper.GameHelper;

@Service
public class GameServiceImpl implements GameService {
	
	@Autowired
	private GameHelper gameHelper;	
	
	@Autowired
	private GameRepository gameManager;
	
	@Override
	public GameResponse addGame(GameRequest gamerequest) {		
		
		gameHelper.checkearDuplicadoGame(getGameBd(gamerequest.getTitle()));
		
		Game game = gameHelper.convertGameRequestToGame(gamerequest);
		gameManager.save(game);
		return new GameResponse(game.getTitle());
	}

	@Override
	public GameRequest getGame(String title) {		
		
		return gameHelper.convertIfExistGame(getGameBd(title));
	}

	@Override
	public GameResponse updateGame(GameRequest gamerequest) {
		Optional<Game> gameBd = getGameBd(gamerequest.getTitle());
		if (gameBd.isPresent()) {
			Game game = gameHelper.convertGameRequestToGame(gamerequest);
			game.setIdgame(gameBd.get().getIdgame());
			gameManager.save(game);
		}else {
			throw new GameKONotExistException();
		}
		return new GameResponse(gamerequest.getTitle());
	}

	@Override
	public GameResponse deleteGame(String title) {
		Optional<Game> gameBd = getGameBd(title);
		if (gameBd.isPresent()) {			
			gameManager.delete(gameBd.get());
		}else {
			throw new GameKONotExistException();
		}		
		return new GameResponse(title);
	}

	@Override
	public Optional<Game> getGameBd(String title) {

		return gameManager.findByTitle(title);
	}

}
