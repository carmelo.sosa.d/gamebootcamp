package com.bootcamp.game.app.services.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bootcamp.game.app.dtos.GenreEnum;
import com.bootcamp.game.app.model.entity.Genre;
import com.bootcamp.game.app.model.repository.GenreRepository;
import com.bootcamp.game.app.services.GenreService;

@Service
public class GenreServiceImpl implements GenreService {

	@Autowired
	private GenreRepository genreManager; 
	
	@Override
	public Optional<Genre> checkGenreIfExist(GenreEnum genre) {
		Optional<Genre> genreBd = genreManager.findByNombre(genre);
		return genreBd;
	}

	@Override
	public Genre addGenre(GenreEnum genre) {
		Genre genero = new Genre();
		genero.setNombre(genre);
		return genreManager.save(genero);		
	}

}
