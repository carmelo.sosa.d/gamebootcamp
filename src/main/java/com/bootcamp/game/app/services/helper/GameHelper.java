package com.bootcamp.game.app.services.helper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.bootcamp.game.app.dtos.GameRequest;
import com.bootcamp.game.app.dtos.GenreEnum;
import com.bootcamp.game.app.exceptions.GameKODuplicadoException;
import com.bootcamp.game.app.exceptions.GameKONotExistException;
import com.bootcamp.game.app.model.entity.Game;
import com.bootcamp.game.app.model.entity.Genre;

@Service
public class GameHelper {
	
	@Autowired
	private ConversionService converter;	
	
	public GameRequest convertIfExistGame(Optional<Game> game) {
		if (game.isPresent()) {
			Game objgame = game.get(); 
			GameRequest gamerequest = converter.convert(objgame, GameRequest.class);
			List<GenreEnum> generos = objgame.getGenres().stream().map(g -> converter.convert(g,GenreEnum.class)).collect(Collectors.toList());
			gamerequest.setGenresreq(generos);
			return gamerequest;
		}else {
			throw new GameKONotExistException();
		}
	}
	
	public void checkearDuplicadoGame(Optional<Game> game) {
		if (game.isPresent()) {
			throw new GameKODuplicadoException();
		}
	}
	
	public void checkearExisteGame(Optional<Game> game) {
		if (!game.isPresent()) {
			throw new GameKONotExistException();
		}
	}
	
	public Game convertGameRequestToGame(GameRequest gamerequest) {
		Game game = converter.convert(gamerequest, Game.class);
		game.setGenres(gamerequest.getGenresreq().stream().map(g -> converter.convert(g, Genre.class)).collect(Collectors.toSet()));
		return game;
	}
	
}
