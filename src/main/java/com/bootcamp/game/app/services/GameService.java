package com.bootcamp.game.app.services;

import java.util.Optional;

import com.bootcamp.game.app.dtos.GameRequest;
import com.bootcamp.game.app.dtos.GameResponse;
import com.bootcamp.game.app.model.entity.Game;

public interface GameService {

	public GameResponse addGame(GameRequest game);
	public GameRequest getGame(String title);
	public GameResponse updateGame(GameRequest game);
	public GameResponse deleteGame(String title);
	public Optional<Game> getGameBd(String title);
}
