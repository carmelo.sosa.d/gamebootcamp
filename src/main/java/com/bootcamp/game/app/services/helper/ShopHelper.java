package com.bootcamp.game.app.services.helper;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import com.bootcamp.game.app.dtos.ShopRequest;
import com.bootcamp.game.app.dtos.StockRequest;
import com.bootcamp.game.app.exceptions.GameKONotExistException;
import com.bootcamp.game.app.exceptions.ShopKODuplicadoException;
import com.bootcamp.game.app.exceptions.ShopKONotExistException;
import com.bootcamp.game.app.exceptions.ShopKOStockException;
import com.bootcamp.game.app.model.entity.Shop;
import com.bootcamp.game.app.services.GameService;
import com.bootcamp.game.app.model.entity.Game;
import com.bootcamp.game.app.model.entity.GameShopStock;

@Service
public class ShopHelper {
	
	@Autowired
	private ConversionService converter;
	
	public ShopRequest convertIfExistShop(Optional<Shop> shop) {
		if (shop.isPresent()) {
			Shop objshop = shop.get();
			ShopRequest shoprequest = converter.convert(objshop, ShopRequest.class);
			shoprequest.setStocks(shop.get().getGameShopStock().stream().map(g -> converter.convert(g, StockRequest.class)).collect(Collectors.toList()));
			return shoprequest;
		}else {
			throw new ShopKONotExistException();
		}
	}
	
	
	public void checkearDuplicadoShop(Optional<Shop> shop) {
		if (shop.isPresent()) {
			throw new ShopKODuplicadoException();
		}
	}
	
	public void checkearExisteShop(Optional<Shop> shop) {
		if (!shop.isPresent()) {
			throw new ShopKONotExistException();
		}
	}
	
	public Shop convertShopRequestToShop(ShopRequest shoprequest, GameService gameService) {
		Shop shop = converter.convert(shoprequest, Shop.class);
		shop.setGameShopStock(shoprequest.getStocks().stream().map(g -> convertStockRequestToGameShopStock(g,gameService)).collect(Collectors.toList()));
		//Add shop a cada stock nuevo
		shop.getGameShopStock().stream().forEach(g -> g.setShop(shop));
		return shop;
	}
	
	public GameShopStock convertStockRequestToGameShopStock(StockRequest source, GameService gameService) {
		Game game;
		Optional<Game> gameBd = gameService.getGameBd(source.getTitleGame());
		if (gameBd.isPresent()) {
			game = gameBd.get();
		}else {
			throw new GameKONotExistException();
		}
		
		GameShopStock stock = new GameShopStock();
		stock.setGame(game);
		stock.setCantidad(source.getCantidad());
		return stock;
	}
	
	public GameShopStock updateCantidadStock(Optional<GameShopStock> gameShopStock, Optional<Game> game, Optional<Shop> shop, int cantidad, boolean esRemove) {
		if (gameShopStock.isPresent()) {
			if (esRemove) {
				int nuevaCantidad = gameShopStock.get().getCantidad()-cantidad;
				if (nuevaCantidad<0) 
					throw new ShopKOStockException();					
				gameShopStock.get().setCantidad(gameShopStock.get().getCantidad()-cantidad);
			}else {
				gameShopStock.get().setCantidad(gameShopStock.get().getCantidad()+cantidad);
			}
			return gameShopStock.get();
		}else {
			if (!esRemove) {
				GameShopStock gameShopStockNuevo = new GameShopStock();
				gameShopStockNuevo.setGame(game.get());
				gameShopStockNuevo.setShop(shop.get());			
				gameShopStockNuevo.setCantidad(cantidad);
				return gameShopStockNuevo;
			}else {
				throw new ShopKOStockException();
			}
		}		
	}
	
	
	
}
